import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { UserinfoComponent } from './user/userinfo/userinfo.component';
import { UserloginComponent } from './user/userlogin/userlogin.component';
import { UserregistrationComponent } from './user/userregistration/userregistration.component';


const routes: Routes = [
  {
    path: '',
    component: UserloginComponent
  },
  {
    path: 'userlogin',
    component: UserloginComponent
  },
  {
    path: 'userregistration',
    component: UserregistrationComponent
  },
  {
    path: 'userinfo',
    component: UserinfoComponent
  }

];

@NgModule({
  declarations:[
    UserloginComponent,
    UserregistrationComponent,
    UserinfoComponent],
  imports: [
    RouterModule.forChild(routes),
    // BrowserModule,
    FormsModule
  ],
})
export class UserModule { }
