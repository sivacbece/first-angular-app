import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { AdminloginComponent } from './admin/adminlogin/adminlogin.component';
import { AdminregistrationComponent } from './admin/adminregistration/adminregistration.component';
import { AdmininfoComponent } from './admin/admininfo/admininfo.component';
import {MaterialModule} from './material-module';
import { SharedService } from './services/shared.service';
import { CommonModule } from '@angular/common';


const routes: Routes = [
  {
    path: '',
    component: AdminloginComponent
  },
  {
    path: 'adminlogin',
    component: AdminloginComponent
  },
  {
    path: 'adminregistration',
    component: AdminregistrationComponent
  },
  {
    path: 'admininfo',
    component: AdmininfoComponent
  }
];

@NgModule({
  declarations:[
    AdminloginComponent,
    AdminregistrationComponent,
    AdmininfoComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    MaterialModule,
    // BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule
  ],
  providers:[SharedService]
})
export class AdminModule { }
