import { Component, OnInit, Input,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-homechild',
  templateUrl: './homechild.component.html',
  styleUrls: ['./homechild.component.scss']
})
export class HomechildComponent implements OnInit {

  @Input() parentMessage:string;
  @Output() messageEvent = new EventEmitter<string>();

  ChildComponentVariable:string="HomechildComponent";
  constructor() { }

  ngOnInit() {
  }

  // ngAfterViewChecked() {
  //   this.messageEvent.emit(this.ChildComponentVariable)
  // }
}
