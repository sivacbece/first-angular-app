import { Injectable} from '@angular/core';
import { HttpClient,HttpResponse,HttpHeaders  } from '@angular/common/http';
import {employee,config } from '../types/shared.types'
import { Observable } from 'rxjs';
import { JsonPipe } from '@angular/common';


// const localUrl = 'assets/employee.json';
// const APIBaseURL='http://localhost:51934/';
// const APIURL="http://localhost:51934/api/employees";
// const saveEmpURL="http://localhost:51934/api/Postemployee";
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

const configURL="assets/config.json";

@Injectable({
  providedIn: 'root'
})
export class SharedService {
private config:config;
  constructor(private http: HttpClient) {
    this.getConfig().subscribe(data => {
      this.config=data;
      console.log(this.config);
     });
   }

  //  getSmartphone() {
  //   return this.http.get(this.config.employeeJSONURL);
  // }
  // getEmployees(): Observable<HttpResponse<employee[]>> {
  //   return this.http.get<employee[]>(
  //     this.config.employeeJSONURL, { observe: 'response' });
  // }
  getEmployees():Observable<any>{
    // return this.http.get<employee[]>( this.config.employeeJSONURL, { observe: 'response' });
    return this.http.get<employee[]>( this.config.APIBaseURL+"employees", { observe: 'response' });
  }
  getEmployeeByID(id:number):Observable<any>{
    return this.http.get<employee>( this.config.APIBaseURL+"employees?id="+id, { observe: 'response' });
  }
  saveEmployee(obj:any):Observable<any>{
  return this.http.post<any>(this.config.APIBaseURL+"employees",obj);
  }
  deleteEmployees(id:number):Observable<any>{
    return this.http.delete<any>( this.config.APIBaseURL+"employees?id="+id, { observe: 'response' });
  }

  getConfig(): Observable<config> {
    return this.http.get<config>(configURL);
  }
}
