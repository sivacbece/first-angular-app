import { Component, OnInit, ViewChild } from '@angular/core';
import { HomechildComponent } from './../homechild/homechild.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  @ViewChild(HomechildComponent, { static: false }) child: HomechildComponent;
  constructor() { }
  // HomechildComponent


  public message: string = "home works";
  public numaricValue: number = 5;
  public numaricValue1: number = 5;
  LabelColor: string = "colorBlue";
  isShowLable: boolean = false;
  buttonDisabled: boolean = false;
  buttonwidth: number = 100;
  HEROES = [
    { id: 1, name: 'Superman' },
    { id: 2, name: 'Batman' },
    { id: 5, name: 'BatGirl' },
    { id: 3, name: 'Robin' },
    { id: 4, name: 'Flash' }
  ];
  people: any[] = [
    {
      "name": "Douglas  Pace",
      "age": 35,
      "country": 'MARS'
    },
    {
      "name": "Mcleod  Mueller",
      "age": 32,
      "country": 'USA'
    },
    {
      "name": "Day  Meyers",
      "age": 21,
      "country": 'HK'
    },
    {
      "name": "Aguirre  Ellis",
      "age": 34,
      "country": 'UK'
    },
    {
      "name": "Cook  Tyson",
      "age": 32,
      "country": 'USA'
    }
  ];
  ngOnInit() {
  }
  // ngAfterViewChecked() {
  //   this.message = this.child.ChildComponentVariable
  // }
  clickMe() {
    if (this.buttonDisabled == false) {
      console.log(this.message);
      alert("ButtonClicked");
    }
    this.buttonwidth = 200;
  }

  receiveMessage($event) {
    this.message = $event
  }
}
