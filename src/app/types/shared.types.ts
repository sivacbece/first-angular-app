export interface employee {
    id: number;
    employee_name: string;
    username: string;
    email_address: string;
    phone: string;
    gender: string;
    employee_address: string;
    city: string;
    created_date: Date;
    modified_date: Date;
    is_active: boolean;
}
export interface employeelist {
    employeeList: [employee];
}

export interface config{
    APIBaseURL:string;
    APIKey:string;
    employeeJSONURL:string;
}