import { Component, OnInit, ViewChild } from '@angular/core';
import { SharedService } from '../../services/shared.service';
import { employee } from '../../types/shared.types'
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-admininfo',
  templateUrl: './admininfo.component.html',
  styleUrls: ['./admininfo.component.scss'],
  providers: [SharedService]
})
export class AdmininfoComponent implements OnInit {
  displayedColumns: string[] = ['id', 'employee_name', 'username', 'email_address', 'phone', 'gender', 'employee_address', 'city', 'edit', 'delete'];
  dataSource = new MatTableDataSource<employee>([]);
  // getDate: Date = new Date();
  constructor(private sharedService: SharedService, private router: Router, private toastr: ToastrService,
    private spinner: NgxSpinnerService) {
  }

  ngOnInit() {
    setTimeout(() => {
      this.getEmployees();
    }, 500);

  }
  getEmployees() {
    this.spinner.show();
    this.sharedService.getEmployees()
      .subscribe(data => {
        console.log(data.body);
        this.dataSource = data.body;
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.toastr.error(err);
      });

  }
  navigateToAdminReg() {
    this.router.navigate(['/admin/adminregistration']);
  }
  editEmployee(row: employee) {
    this.router.navigate(['/admin/adminregistration'], { queryParams: { id: row.id } });
  }
  DeleteEmployee(row: employee) {
    this.spinner.show();
    this.sharedService.deleteEmployees(row.id)
      .subscribe(data => {
        this.spinner.hide();
        this.toastr.success("Deleted Successfully..!");
        this.getEmployees();
      }, err => {
        this.spinner.hide();
        this.toastr.error(err);
      });

  }
}
