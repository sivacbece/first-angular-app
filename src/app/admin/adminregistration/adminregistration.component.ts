import { Component, OnInit } from '@angular/core';
import {employee} from '../../types/shared.types';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { SharedService } from '../../services/shared.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-adminregistration',
  templateUrl: './adminregistration.component.html',
  styleUrls: ['./adminregistration.component.scss'],
  providers:[SharedService]
})
export class AdminregistrationComponent implements OnInit {
  employeeForm = new FormGroup({
    employee_name: new FormControl(),
    username: new FormControl(),
    email_address: new FormControl(),
    phone: new FormControl(),
    gender: new FormControl(),
    employee_address: new FormControl(),
    city: new FormControl(),
  });

  // get first(): any { return this.employeeForm.get('first'); }
  employeeData:employee={
    city:null,
    created_date:null,
    employee_address:null,
    email_address:null,
    employee_name:null,
    gender:null,
    id:null,
    is_active:null,
    modified_date:null,
    phone:null,
    username:null
  };

  constructor(private sharedService:SharedService, 
    private router:Router, 
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute,
    private spinner: NgxSpinnerService ) { }

  ngOnInit() {
      this.activatedRoute.queryParams.subscribe(params => {
        if(params.id){
          setTimeout(() => {
            this.getEmployeeByID(params.id);
          }, 500);
        }
      });

  }

  saveEmployee(){
    this.sharedService.saveEmployee(this.employeeData)
    .subscribe((res: any) => {
      console.log(res);
      this.toastr.success("Saved Successfully");
      this.router.navigate(['/admin/admininfo']);
      }, (err: any) => {
          this.toastr.error(err);
      });
  };
  clearEmployee(){
    this.employeeData={
      city:null,
      created_date:null,
      employee_address:null,
      email_address:null,
      employee_name:null,
      gender:null,
      id:null,
      is_active:null,
      modified_date:null,
      phone:null,
      username:null
    };
  };
  getEmployeeByID(id){
    this.spinner.show();
    this.sharedService.getEmployeeByID(id).subscribe((res: any) => {
      this.employeeData=res.body;
      this.spinner.hide();
      },(err: any) => {
        this.spinner.hide();  
        this.toastr.error(err);
      });
  }
  goToEmployeeList(){
    this.router.navigate(['/admin/admininfo']);
  };

}
